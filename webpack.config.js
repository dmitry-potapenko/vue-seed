const path = require('path');

function resolve(dir) {
  return path.join(__dirname, dir)
}

module.exports = {
  context: __dirname,
  devtool: "source-map",
  entry: __dirname + "/src/main.js",
  output: {
    path: __dirname + "/build",
    filename: "bundle.js"
  },
  resolve: {
    extensions: ['.js', '.vue', '.json'],
    alias: {
      'vue$': 'vue/dist/vue.esm.js',
      '@': resolve('src')
    }
  },
  module: {
    rules: [{
        test: /\.js$/,
        loader: 'babel-loader',
        include: [resolve('src'), resolve('test')]
      },
      {
        test: /\.html$/,
        loader: 'html-loader?exportAsEs6Default'
      }
    ]
  }
}